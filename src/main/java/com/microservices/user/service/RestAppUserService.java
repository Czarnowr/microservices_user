package com.microservices.user.service;

import com.microservices.user.model.AppUser;
import com.microservices.user.model.dto.AppUserDto;
import com.microservices.user.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class RestAppUserService {

    private AppUserRepository appUserRepository;

    @Autowired
    public RestAppUserService(final AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    public Long create(AppUserDto appUserDto) {
        AppUser appUser = appUserRepository.save(mapFromAppUserDto(new AppUser(), appUserDto));

        return appUser.getId();
    }

    public AppUserDto get(final Long id) {
        return mapFromAppUser(new AppUserDto(), getUserFromDatabase(id));
    }

    public AppUserDto update(final Long id, final AppUserDto appUserDto) {
        AppUser appUser = mapFromAppUserDto(getUserFromDatabase(id), appUserDto);

        appUser = appUserRepository.save(appUser);

        return mapFromAppUser(appUserDto, appUser);
    }

    public void delete(final Long id){
        appUserRepository.deleteById(id);
    }

    private AppUser getUserFromDatabase(Long id){
        Optional<AppUser> userOptional = appUserRepository.findById(id);

        if (!userOptional.isPresent()) {
            throw new EntityNotFoundException("User not found");
        }

        return userOptional.get();
    }

    private AppUserDto mapFromAppUser(AppUserDto appUserDto, AppUser appUser) {
        appUserDto.setFirstName(appUser.getFirstName());
        appUserDto.setSurname(appUser.getSurname());
        appUserDto.setLogin(appUser.getLogin());
        return appUserDto;
    }

    private AppUser mapFromAppUserDto(AppUser appUser, AppUserDto appUserDto) {
        appUser.setFirstName(appUserDto.getFirstName());
        appUser.setSurname(appUserDto.getSurname());
        appUser.setLogin(appUserDto.getLogin());
        return appUser;
    }


}
